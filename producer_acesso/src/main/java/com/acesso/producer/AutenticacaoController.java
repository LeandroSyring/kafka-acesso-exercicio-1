package com.acesso.producer;

import com.acesso.LogAcesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@RestController
public class AutenticacaoController {

    @Autowired
    AutenticacaoProducer autenticacaoProducer;

    @GetMapping("/acesso/{cliente}/{porta}")
    public void autenticarPorta(@PathVariable String cliente, @PathVariable String porta){

        Random random = new Random();
        boolean nextBoolean = random.nextBoolean();

        LogAcesso logAcesso = new LogAcesso();
        logAcesso.setAcesso(nextBoolean);
        logAcesso.setCliente(cliente);
        logAcesso.setPorta(porta);
        logAcesso.setDescricaoAcesso(nextBoolean ? "Acesso Liberado" : "Acesso Negado");

        autenticacaoProducer.enviaAoKafka(logAcesso);
    }
}
