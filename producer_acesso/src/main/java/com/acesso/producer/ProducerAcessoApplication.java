package com.acesso.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerAcessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducerAcessoApplication.class, args);
	}

}
