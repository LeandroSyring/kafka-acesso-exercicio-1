package com.acesso.producer;

import com.acesso.LogAcesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Service
public class AutenticacaoProducer {

    @Autowired
    private KafkaTemplate<String, LogAcesso> producer;


    public void enviaAoKafka(LogAcesso logAcesso) {
        producer.send("spec2-leandro-guilherme-2", logAcesso);
    }
}
