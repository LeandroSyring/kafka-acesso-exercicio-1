package com.acesso;

import org.apache.kafka.common.protocol.types.Field;

public class LogAcesso {

    String porta;

    String cliente;

    boolean acesso;

    String descricaoAcesso;

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public boolean isAcesso() {
        return acesso;
    }

    public void setAcesso(boolean acesso) {
        this.acesso = acesso;
    }

    public String getDescricaoAcesso() {
        return descricaoAcesso;
    }

    public void setDescricaoAcesso(String descricaoAcesso) {
        this.descricaoAcesso = descricaoAcesso;
    }
}
