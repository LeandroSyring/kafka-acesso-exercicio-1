package com.acesso.consumer;

import com.acesso.LogAcesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class AutenticacaoConsumer {

    @KafkaListener(topics = "spec2-leandro-guilherme-2", groupId = "grupo-1")
    public void receber(@Payload LogAcesso logAcesso) throws IOException {
        System.out.println("Log Acesso:" + logAcesso.getDescricaoAcesso() + " - Cliente: " + logAcesso.getCliente() + " - Porta: " + logAcesso.getPorta() + ".");


        FileWriter arq = new FileWriter("log_acesso.txt", true);
        PrintWriter gravarArq = new PrintWriter(arq);

        gravarArq.println("Log Acesso:" + logAcesso.getDescricaoAcesso() + " - Cliente: " + logAcesso.getCliente() + " - Porta: " + logAcesso.getPorta() + ". | ");

        arq.close();

    }
}
