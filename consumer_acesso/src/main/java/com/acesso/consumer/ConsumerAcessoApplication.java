package com.acesso.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerAcessoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerAcessoApplication.class, args);
	}

}
